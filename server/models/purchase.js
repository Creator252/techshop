const {
  Schema,
  model
} = require('mongoose');

const purchaseSchema = new Schema({
  status: {
    type: String,
    required: true
  },
  totalPrice: {
    type: Number,
    required: true
  },
  region: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  },
  street: {
    type: String,
    required: true
  },
  home: {
    type: String,
    required: true
  },
  housing: {
    type: String,
    required: true
  },
  comment: {
    type: String,
    required: false
  },
  name: {
    type: String,
    required: true
  },
  sname: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  products: {
    type: Object,
    required: true
  },
  payment: {
    type: String,
    required: true
  },
  cardName: {
    type: String,
    required: false
  },
  cardDate: {
    type: String,
    required: false
  },
  cardNumber: {
    type: String,
    required: false
  },
  cardCvv: {
    type: String,
    required: false
  }
});

module.exports = model('purchases', purchaseSchema);
