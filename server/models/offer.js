const { Schema, model } = require("mongoose");

const offerSchema = new Schema({
  externalId: { type: Number, required: true },
  name: { type: String, required: true },
  category: { type: String, required: true },
  description: { type: String, required: false },
  price: { type: Number, required: false },
  image: { type: String, required: false },
});

module.exports = model("Offer", offerSchema);
