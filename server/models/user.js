const {
  Schema,
  model
} = require("mongoose");

const userSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  userName: {
    type: String,
    required: false
  },
  photo: {
    type: String,
    required: false
  },
  role: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: false
  },
  sname: {
    type: String,
    required: false
  },
  region: {
    type: String,
    required: false
  },
  city: {
    type: String,
    required: false
  },
  street: {
    type: String,
    required: false
  },
  home: {
    type: String,
    required: false
  },
  housing: {
    type: String,
    required: false
  },
  phone: {
    type: String,
    required: false
  },
  image: {
    type: String,
    required: false
  },
});

module.exports = model("User", userSchema);
