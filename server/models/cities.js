const {
  Schema,
  model
} = require('mongoose');

const citiesSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  regions: {
    type: Array,
    required: true
  }
});

module.exports = model('Cities', citiesSchema);
