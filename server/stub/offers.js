const offers = [
  {
    externalId: 1,
    name: "Ноутбук HONOR MagicBook 15 2021 BDR-WFH9HN 53011TAP",
    category: "Laptop",
    description:
      "15.6 1920 x 1080 IPS, 60 Гц, несенсорный, Intel Core i5 1135G7 2400 МГц, 16 ГБ, SSD 512 ГБ, видеокарта встроенная, Windows 10, цвет крышки серый",
    price: 2744,
    image: "",
  },
  {
    externalId: 2,
    name: "Ноутбук ASUS X515MA-EJ017",
    category: "Laptop",
    description:
      "15.6Э 1920 x 1080 TN+Film, 60 Гц, несенсорный, Intel Celeron N4020 1100 МГц, 4 ГБ, SSD 256 ГБ, видеокарта встроенная, Linux, цвет крышки серый",
    price: 865,
    image: "",
  },
  {
    externalId: 3,
    name: "Lenovo IdeaPad 3 15ITL6 82H800JSRE",
    category: "Laptop",
    description:
      "15.6 1920 x 1080 IPS, 60 Гц, несенсорный, Intel Core i5 1135G7 2400 МГц, 8 ГБ, SSD 256 ГБ, видеокарта NVIDIA GeForce MX350 2 ГБ, без ОС, цвет крышки серый",
    price: 1999,
    image: "",
  },
  {
    externalId: 4,
    name: "Ноутбук Apple Macbook Air 13 M1 2020 MGN63",
    category: "Laptop",
    description:
      "13.3 2560 x 1600 IPS, 60 Гц, несенсорный, Apple M1 3200 МГц, 8 ГБ, SSD 256 ГБ, видеокарта встроенная, Mac OS, цвет крышки серый",
    price: 2749,
    image: "",
  },
  {
    externalId: 5,
    name: "Lenovo 3350",
    category: "Laptop",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit.",
    price: 72000,
    image: "",
  },
  {
    externalId: 6,
    name: "Наушники HONOR Earbuds 2 Lite",
    category: "Headphones",
    description:
      "беспроводные наушники с микрофоном, внутриканальные, портативные, Bluetooth, 20-20000 Гц, быстрая зарядка, время работы 10 ч, с кейсом 32 ч, активное шумоподавление",
    price: 159,
    image: "",
  },
  {
    externalId: 7,
    name: "Наушники Xiaomi Redmi Buds 3 Pro WSEJ01ZM",
    category: "Headphones",
    description:
      "беспроводные наушники с микрофоном, внутриканальные, портативные, Bluetooth, 20-20000 Гц, время работы 6 ч, с кейсом 28 ч, активное шумоподавление",
    price: 178,
    image: "",
  },
  {
    externalId: 8,
    name: "Huawei nova 8i NEN-L22 6GB/128GB",
    category: "Mobile",
    description:
      "Android, экран 6.67 IPS (1080x2376), Qualcomm Snapdragon 662, ОЗУ 6 ГБ, флэш-память 128 ГБ, камера 64 Мп, аккумулятор 4300 мАч, 2 SIM",
    price: 1099,
    image: "",
  },
  {
    externalId: 9,
    name: "Samsung Galaxy Z Flip3 5G 8GB/256GB",
    category: "Mobile",
    description:
      "Android, экран 6.7 AMOLED (1080x2640), Qualcomm Snapdragon 888, ОЗУ 8 ГБ, флэш-память 256 ГБ, камера 12 Мп, аккумулятор 3300 мАч, 1 SIM",
    price: 2840,
    image: "",
  },
];

module.exports = offers;
