const multer = require("multer");

const MIME_TYPES = {
  "image/jpg": ".jpg",
  "image/jpeg": ".jpeg",
  "image/png": ".png",
};

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    const isValid = MIME_TYPES[file.mimetype];
    const error = isValid ? null : new Error("Invalid mime type");
    callback(error, "server/uploads");
  },
  filename: (req, file, callback) => {
    const name = file.originalname.toLowerCase().split(" ").join("-");
    callback(null, `${Date.now()}-${name}`);
  }
});
const upload = multer({
  storage: storage
});

module.exports = upload;
