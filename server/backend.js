const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const connectDatabase = require("./db-connection");
const offers = require("./stub/offers");
const offerRoutes = require("./routes/offer.routes");
const userRoutes = require("./routes/user.routes");
const citiesRoutes = require("./routes/cities.routes");
const purchaseRoutes = require("./routes/purchase.routes");

const baseUrl = "/api/v1";
const stubBaseUrl = `${baseUrl}/stub`;

const app = express(); // creates express app
app.use(cors()); // enables CORS
app.use(bodyParser.json()); // parses body to valid json

connectDatabase();

// Defining routes
app.use(`${baseUrl}/offers`, offerRoutes);
app.use(`${baseUrl}/users`, userRoutes);
app.use(express.static('server'))
app.use(`${baseUrl}/cities`, citiesRoutes);
app.use(`${baseUrl}/purchases`, purchaseRoutes);

// Defining stub route
app.get(`${stubBaseUrl}/offers`, (req, res) => {
  res
    .status(200)
    .json({
      message: "Offers fetched successfully!",
      offers: offers
    });
});

module.exports = app;
