const Cities = require("../models/cities");

const getCities = async (req, res) => {
  try {
    const cities = await Cities.find();
    res.status(200).json({
      message: "Cities fetched successfully!",
      cities: cities
    })
  } catch (error) {
    res.status(500).json({
      message: "Internal server error",
      error: error
    });
  }
}

module.exports = {
  getCities
};
