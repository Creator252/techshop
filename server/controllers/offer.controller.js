const Offer = require("../models/offer");
const jwt = require('jsonwebtoken');

const getOffers = async (req, res) => {
  try {
    let offers = []
    if (req.query.name && req.query.category) {
      const s = req.query.name
      const regex = new RegExp(s, 'i')

      offers = await Offer.find(
        {$and: [{name: {$regex: regex}}, {category: req.query.category}]}
      );
      return res
        .status(200)
        .json({message: "Offers fetched successfully!", offers: offers});

    }

    if (req.query.maxPrice && req.query.minPrice && req.query.category) {
      offers = await Offer.find({
        $and: [
          {price: {$lte: req.query.maxPrice}},
          {price: {$gte: req.query.minPrice}},
          {category: req.query.category},
        ]
      })
      return res
        .status(200)
        .json({message: "Offers fetched successfully!", offers: offers});
    }


    if (req.query.category) {
      offers = await Offer.find({category: req.query.category})
    } else {
      offers = await Offer.find();
    }
    if (req.query.name) {
      const s = req.query.name
      const regex = new RegExp(s, 'i')
      offers = await Offer.find({name: {$regex: regex}});
    }
    if (req.query.maxPrice && req.query.minPrice) {
      offers = await Offer.find({
        $and: [
          {price: {$lte: req.query.maxPrice}},
          {price: {$gte: req.query.minPrice}},
        ]
      })
    }
    res
      .status(200)
      .json({message: "Offers fetched successfully!", offers: offers});
  } catch (error) {
    res.status(500).json({message: "Internal server error", error: error});
  }
};

const getOfferById = async (req, res) => {
  try {
    const offer = await Offer.find({externalId: req.params.id});
    if (offer?.length) {
      res.status(200).json(offer[0]);
    } else {
      res.status(404).json({message: "Offer not found"});
    }
  } catch (error) {
    res.status(500).json({message: "Internal server error", error: error});
  }
};

const getOffersFromCategory = async (req, res) => {
  try {
    const offers = await Offer.find({category: req.params.categoryName});
    if (offers?.length) {
      res.status(200).json({offers: offers});
    } else {
      res.status(404).json({message: `Offers not found in ${categoryName} category`});
    }
  } catch (error) {
    res.status(500).json({message: "Internal server error", error: error});
  }
};


const createOffer = async (req, res) => {
  try {
    const token = req.headers.authorization.split(' ')[1]
    const {role: role} = jwt.verify(token, "shhhhh")
    if (role !== "admin") {
      return res.status(401).json({message: "No access"})
    }
    const imageUrl = `${req.protocol}://${req.get("host")}/uploads/${req.file.filename}`
    // const offer = new Offer({...req.body});
    const offer = new Offer({
      externalId: req.body.externalId,
      name: req.body.name,
      category: req.body.category,
      description: req.body.description,
      price: req.body.price,
      image: imageUrl,
    })
    console.log("offer", offer);
    await offer.save();
    res.status(201).json({message: "Offer added successfully!"});
  } catch (error) {
    res.status(500).json({message: "Internal server error", error: error});
  }
};

const deleteOffer = async (req, res) => {
  try {
    const token = req.headers.authorization.split(' ')[1]
    const {role: role} = jwt.verify(token, "shhhhh")
    if (role !== "admin") {
      return res.status(401).json({message: "No access"})
    }
    const {id} = req.params;
    await Offer.remove({externalId: id})

    res.status(200).json({message: "Offer deleted successfully!"});
  } catch (error) {
    res.status(500).json({message: "Internal server error", error: error});
  }
};

const editOffer = async (req, res) => {
  try {
    const token = req.headers.authorization.split(' ')[1]
    const {role: role} = jwt.verify(token, "shhhhh")
    if (role !== "admin") {
      return res.status(401).json({message: "No access"})
    }
    const {id} = req.params;
    const imageUrl = `${req.protocol}://${req.get("host")}/uploads/${req.file.filename}`
    await Offer.findOneAndUpdate(
      {externalId: id},
      {
        $set: {
          name: req.body.name,
          category: req.body.category,
          description: req.body.description,
          price: req.body.price,
          image: imageUrl,
        }
      },
    )
    res.status(200).json({message: "Offer been edit!"});
  } catch (error) {
    res.status(500).json({message: "Internal server error", error: error});
  }
};


module.exports = {
  getOffers,
  getOfferById,
  createOffer,
  getOffersFromCategory,
  deleteOffer,
  editOffer,
};
