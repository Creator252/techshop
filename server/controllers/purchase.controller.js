const Purchase = require("../models/purchase");
const jwt = require('jsonwebtoken');

const getAllPurchase = async (req, res) => {
  const token = req.headers.authorization.split(' ')[1]
  const {role: role} = jwt.verify(token, "shhhhh")
  if (role !== "admin") {
    return res.status(401).json({message: "No access"})
  }
  let purchase = []
  let totalPurchase = await Purchase.count()
  try {
    if (req.query.currentPage) {
      const currentPage = parseInt(req.query.currentPage);
      const limit = parseInt(req.query.limit);
      const skipIndex = (currentPage - 1) * limit;
      purchase = await Purchase.find()
        .sort({_id: 1})
        .limit(limit)
        .skip(skipIndex)
        .exec();
    } else purchase = await Purchase.find();

    if (req.query.email) {
      const s = req.query.email
      const regex = new RegExp(s, 'i')
      purchase = await Purchase.find({email: {$regex: regex}})
      totalPurchase = purchase.length
    }
    res.status(200).json({purchase, totalPurchase});
  } catch (error) {
    res.status(500).json({
      message: "Internal server error",
      error: error
    });
  }
}

const createPurchase = async (req, res) => {
  try {
    const purchase = new Purchase({
      status: req.body.status,
      totalPrice: req.body.totalPrice,
      region: req.body.region,
      city: req.body.city,
      street: req.body.street,
      home: req.body.home,
      housing: req.body.housing,
      comment: req.body.comment,
      name: req.body.name,
      sname: req.body.sname,
      email: req.body.email,
      products: req.body.products,
      payment: req.body.payment,
      cardName: req.body.cardName,
      cardDate: req.body.cardDate,
      cardNumber: req.body.cardNumber,
      cardCvv: req.body.cardCvv,
    });
    if (purchase.email != null) {
      await purchase.save();
      res.status(201).json({
        message: "order created"
      });
    }
  } catch (error) {
    res.status(500).send("server error");
  }
}

const getPurchaseByEmail = async (req, res) => {
  try {
    const purchase = await Purchase.find({
      email: req.params.email
    });
    if (purchase.length) {
      res.status(200).json({
        message: "Purchases fetched successfully!",
        purchases: purchase
      })
    } else {
      res.status(404).json({
        message: "not purchases"
      })
    }
  } catch (error) {
    res.status(500).json(
      "error"
    );
  }
}

const changeStatus = async (req, res) => {
  try {
    const token = req.headers.authorization.split(' ')[1]
    const {role: role} = jwt.verify(token, "shhhhh")
    if (role !== "admin") {
      return res.status(401).json({message: "No access"})
    }
    const _id = req.params._id;
    await Purchase.findOneAndUpdate(
      {_id: _id},
      {
        $set: {
          status: req.body.status
        }
      },
    )
    res.status(200).json({message: "Change successful!"});
  } catch (error) {
    res.status(500).json({message: "Internal server error", error: error});
  }
};

module.exports = {
  getAllPurchase,
  createPurchase,
  getPurchaseByEmail,
  changeStatus,
};
