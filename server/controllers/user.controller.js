const User = require("../models/user");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const getUserByEmail = async (req, res) => {
  try {
    const user = await User.find({
      email: req.params.email
    });
    if (user.length) {
      res.status(200).json(user[0]);
    } else {
      res.status(404).json({
        message: "User not found"
      });
    }
  } catch (error) {
    res.status(500).json({
      message: "Internal server error",
      error: error
    });
  }
};


const loginUser = async (req, res) => {
  try {
    const user = await User.find({
      email: req.body.email
    })
    const matchPassword = await bcrypt.compare(req.body.password, user[0].password)
    if (user.length === 0) {
      return res.status(403).json({
        message: "User not found"
      })
    }
    // const matchPassword = await bcrypt.compare(req.body.password, user[0].password)
    else if (!matchPassword) {
      return res.status(403).json({
        message: "Password incorrect"
      })
    }

    let token = jwt.sign({
      email: req.body.email,
      userName: user[0].userName,
      role: user[0].role
    }, "shhhhh", {
      expiresIn: '4h'
    });

    res.status(200).json({
      token: token,
      email: req.body.email,
      role: user[0].role,
    })

  } catch (error) {
    res.status(500).json({
      message: "Username or password incorrect"
    })
  }
};

const registerUser = async (req, res) => {
  try {

    const matchEmail = await User.find({
      email: req.body.email
    })
    if (matchEmail.length !== 0) {
      return res.status(403).json({
        message: "Email занят"
      })
    } else {
      const user = new User({
        ...req.body
      });
      user.email = req.body.email
      user.userName = user.email.split("@")[0];
      user.password = await bcrypt.hash(req.body.password, 12);
      user.role = "user";
      user.image = "http://localhost:3000/uploads/1642844758202-boy-avatar.png";
      await user.save();
      console.log("user", user);
      let token = jwt.sign({
        email: req.body.email,
        userName: user.userName,
        role: user.role
      }, "shhhhh", {
        expiresIn: '4h'
      });

      return res.status(200).json({
        message: "User added successfully!",
        token: token,
        userName: user.userName,
        role: user.role,
        email: user.email,
      });

    }
  } catch (error) {
    res.status(500).json({
      message: "Internal server error",
      error: error
    });
  }
}

const editUser = async (req, res) => {
  try {
    const {
      email
    } = req.params;
    const imageUrl = `${req.protocol}://${req.get("host")}/uploads/${req.file.filename}`;
    await User.findOneAndUpdate({
      email
    }, {
      $set: {
        name: req.body.name,
        sname: req.body.sname,
        region: req.body.region,
        city: req.body.city,
        street: req.body.street,
        home: req.body.home,
        housing: req.body.housing,
        phone: req.body.phone,
        image: imageUrl
      }
    })
    res.status(200).json({
      message: "User been edit!"
    });
  } catch {
    res.status(500).json({
      message: "Internal server error",
      error: error
    });
  }
}

module.exports = {
  getUserByEmail,
  loginUser,
  registerUser,
  editUser
};
