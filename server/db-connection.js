const mongoose = require("mongoose");

const dbUser = "admin";
const dbPass = "admin";
const dbUrl = `mongodb+srv://${dbUser}:${dbPass}@cluster0.hcxca.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;
const connectDatabase = () => {
  mongoose
    .connect(dbUrl)
    .then(() => console.log("Connected to DB!"))
    .catch((error) => {
      console.error("Connection failed!", error.message);
      process.exit(1); // Exit process with failure
    });
};

module.exports = connectDatabase;
