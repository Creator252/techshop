const express = require("express");
const router = express.Router();
const imageUpload = require("../middleware/image-upload")
const offerController = require("../controllers/offer.controller")

router.get("/", offerController.getOffers);
router.post("/", imageUpload.single("image"), offerController.createOffer);
router.get("/:id", offerController.getOfferById);
router.get("/category/:categoryName", offerController.getOffersFromCategory);
router.delete("/delete/:id", offerController.deleteOffer);
router.put("/edit/:id", imageUpload.single("image"), offerController.editOffer);

module.exports = router;
