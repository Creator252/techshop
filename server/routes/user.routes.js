const express = require("express");
const router = express.Router();
const imageUpload = require("../middleware/image-upload");

const userController = require("../controllers/user.controller")
const {
  getUser
} = require("../controllers/user.controller");

router.get("/:email", userController.getUserByEmail);
router.post("/login", userController.loginUser);
router.post("/register", userController.registerUser);
router.put("/edit/:email", imageUpload.single("image"), userController.editUser);

module.exports = router;
