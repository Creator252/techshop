const express = require('express');
const router = express.Router();

const citiesController = require("../controllers/cities.controller");

router.get("/", citiesController.getCities);

module.exports = router;
