const express = require('express');
const router = express.Router();

const purchaseController = require("../controllers/purchase.controller");

router.get("/all", purchaseController.getAllPurchase);
// router.get("/:id", purchaseController.getSinglePurchase);
router.post("/", purchaseController.createPurchase);
router.get("/:email", purchaseController.getPurchaseByEmail);
router.put("/editStatus/:_id", purchaseController.changeStatus);

module.exports = router;
