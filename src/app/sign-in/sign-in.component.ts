import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from "../services/user.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import {emailValidator} from '../../app/user-info/email.validator';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.less']
})
export class SignInComponent implements OnInit {

  public hide = true;
  public loginForm?: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService,
              private _snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email,emailValidator.validEmails]],
      password: ["", [Validators.required, Validators.minLength(6)]],
    })
  }

  public get email() {
    return this.loginForm?.get("email")
  }

  public get password() {
    return this.loginForm?.get("password")
  }

  public signIn(): void {
    const {email, password} = this.loginForm?.value
    this.userService.login(email, password).subscribe(() => {
        if (localStorage.getItem('role') === 'admin') {
          this.router.navigate(["admin-panel"])
        } else if (localStorage.getItem('role') === 'user') {
          this.router.navigate(["profile"])
        }
        this._snackBar.open('Успешно авторизированы', 'Close', {
          duration: 3000
        });
      },
      (error) => {
        this._snackBar.open(error.message, 'Close', {
          duration: 3000
        });
      })
  }

}
