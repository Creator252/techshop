import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SliderComponent } from './slider/slider.component';
import { FilterComponent } from './filter/filter.component';
import { CategoryComponent } from './category/category.component';
import { OfferListComponent } from './offer-list/offer-list.component';
import { FooterComponent } from './footer/footer.component';
import { CartService } from "./services/cart.service";
import { CartComponent } from './cart/cart.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MainComponent } from './main/main.component';
import { OffersService } from "./services/offers.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FavoritesComponent } from './favorites/favorites.component';
import { OfferComponent } from './offer/offer.component';
import { ReactiveFormsModule } from "@angular/forms";
import { RxReactiveFormsModule } from '@rxweb/reactive-form-validators';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ProfileComponent } from './profile/profile.component';
import { JwtModule } from "@auth0/angular-jwt";
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { UserInfoComponent } from './user-info/user-info.component';
import { NgxMaskModule } from 'ngx-mask';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { MatExpansionModule } from '@angular/material/expansion';
import { NgImageSliderModule } from 'ng-image-slider';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SliderComponent,
    FilterComponent,
    CategoryComponent,
    OfferListComponent,
    FooterComponent,
    CartComponent,
    NotFoundComponent,
    MainComponent,
    SignInComponent,
    SignUpComponent,
    PurchaseComponent,
    FavoritesComponent,
    OfferComponent,
    ProfileComponent,
    UserInfoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatBadgeModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    NgImageSliderModule,
    RxReactiveFormsModule,
    MatIconModule,
    MatSnackBarModule,
    MatTabsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem("token");
        },
      },
    }),
    FormsModule,
    MatSelectModule,
    NgxMaskModule.forRoot(),
    NgxSliderModule,
    MatExpansionModule,
  ],
  providers: [
    CartService,
    OffersService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
