import { Observable } from 'rxjs';
import { CategoriesService } from '../services/categories.service';
import { CategoryModel } from '../models/category.model';
import { OffersService } from '../services/offers.service';
import { OfferInterface, OfferServerResponse } from '../models/offer.model';
import { Component, OnInit } from '@angular/core';
import { CartService } from "../services/cart.service";
import { Router } from '@angular/router';
import { FavoritesService } from '../services/favorites.service';
import { FavoriteInterface } from '../models/FavoriteInterface.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.less']
})
export class OfferListComponent implements OnInit {

  public offerList$: Observable<OfferInterface[]> | undefined
  public favoriteIdsObservable: Observable<number[]> | undefined;


  constructor(
    private userService: UserService,
    private cartService: CartService,
    private offersService: OffersService,
    private categoriesService: CategoriesService,
    private router: Router,
    private favoritesService: FavoritesService,
    private _snackBar: MatSnackBar,
  ) {
  }

  addToCart(id: any) {
    this.cartService.addToCart(id)
    this._snackBar.open('Добавлено', 'ok', {
      duration: 2000,
    });
  }

  addFavorite(favorite: any, id: any) {
    if (this.userService.isAuthenticated()) {
      this.favoritesService.addFavorite(favorite, id);
      this._snackBar.open('Добавлено', 'ok', {
        duration: 2000,
      });
    } else {
      this._snackBar.open('Избранное доступно только зарегистрированным пользователям', 'ok', {
        duration: 3500,
      })
    }
  }

  delFavorite(favorite: any, id: any) {
    this.favoritesService.delFavorite(favorite, id)
    this._snackBar.open('Удалено', 'ok', {
      duration: 3500,
    })
  }

  toggleFavorite(item: FavoriteInterface, externalId: number, favIds: any): void {
    if (favIds.includes(externalId)) {
      this.delFavorite(item, externalId);
    } else {
      this.addFavorite(item, externalId);
    }
  }

  checkActive(id: number, favIds: any): boolean {
    return favIds.includes(id);
  }

  getCategories(): CategoryModel[] {
    return this.categoriesService.getCategories()
  }

  ngOnInit(): void {
    this.offerList$ = this.offersService.getOffersData$()
    this.favoriteIdsObservable = this.favoritesService.getFavoriteIds$();
  }

  selectOffer(id: Number) {
    this.offersService.selectOffer(id)
  }

}
