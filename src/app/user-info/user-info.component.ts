import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { FormControl, FormGroup} from '@angular/forms';
import { FormControlName, ReactiveFormsModule, Validators,FormsModule } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { mimeTypeValidator } from './mime-type.validator';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.less']
})


export class UserInfoComponent implements OnInit {
  public email = localStorage.getItem('email');
  public user$: Observable<any> | undefined;
  public userDataForm: FormGroup;
  public _imagePreview: string | undefined;
  public src : string = '';

  constructor(private userService: UserService, private router: Router) {
    this.userDataForm = new FormGroup({
      image: new FormControl(null, [],mimeTypeValidator),
      name: new FormControl(null,[Validators.pattern(/^[a-zA-Z]*|[а-яА-Я]*$/)]),
      sname: new FormControl(null,[Validators.pattern(/^[a-zA-Z]*|[а-яА-Я]*$/)] ),
      region: new FormControl(null),
      city: new FormControl(null),
      street: new FormControl(null, [Validators.pattern(/^[a-zA-Z]*|[а-яА-Я]*$/)]),
      home: new FormControl(null),
      housing: new FormControl(null),
      phone: new FormControl(null,[Validators.pattern(/^[0-9]*$/)])
    });
   }

   public editUser(): void{
      this.userService.editUser(
      this.userDataForm?.value.image,
       this.userDataForm?.value.name,
       this.userDataForm?.value.sname,
       this.userDataForm?.value.region,
       this.userDataForm?.value.city,
       this.userDataForm?.value.street,
       this.userDataForm?.value.home,
       this.userDataForm?.value.housing,
       this.userDataForm?.value.phone,
     ).subscribe((data:any)=> {
      if(data.message == 'User been edit!'){
        this.router.navigate(["profile"])
      }
    });
   }

   get _phone(){
    return this.userDataForm?.get('phone');
   }

  ngOnInit(): void {
    this.user$ = this.userService.getUserByEmail();
    this.user$?.forEach(async(user) => {
      const response = await fetch(user.image);
        const blob = await response.blob();
        const file = new File([blob], 'image.jpg', {type: blob.type});
      this.userDataForm.patchValue({
      name: user.name,
      sname:user.sname ,
      region: user.region,
      city: user.city,
      street:user.street,
      home: user.home,
      housing: user.housing,
      phone: user.phone,
      image: file
    })
    this.src = user.image;

  });

  }

  public _onImagePicked(event: Event): void {
    const files = (event.target as HTMLInputElement).files;
    const file = files?.length && files[0];
    this.userDataForm?.patchValue({image: file});
    this.userDataForm?.get("image")?.updateValueAndValidity();
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        this._imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }
}
