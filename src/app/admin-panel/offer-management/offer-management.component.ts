import { Component, OnInit } from '@angular/core';
import { UserService } from "../../services/user.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from "@angular/material/snack-bar";
import { OffersService } from "../../services/offers.service";
import { Observable } from 'rxjs';
import { switchMap, tap, map, filter } from 'rxjs/operators';
import { OfferInterface } from "../../models/offer.model";

@Component({
  selector: 'app-offer-management',
  templateUrl: './offer-management.component.html',
  styleUrls: ['./offer-management.component.less']
})
export class OfferManagementComponent implements OnInit {

  public createOfferForm: FormGroup | undefined;
  public _imagePreview: string | undefined;
  public offerList$: Observable<any> | undefined;
  public displayedColumns: string[] = ['position', 'image', 'name', 'category', 'description', 'price', 'delete', 'edit'];
  public offerForEdit: any | undefined;
  public isEdit: boolean = false

  offerData: OfferInterface[] = []

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private userService: UserService,
              private _snackBar: MatSnackBar,
              private offersService: OffersService,
  ) {
  }


  ngOnInit(): void {
    this.createOfferForm = this.formBuilder.group({
      externalId: ["", [Validators.required]],
      name: ["", [Validators.required]],
      category: ["", [Validators.required]],
      description: ["", []],
      price: ["", [Validators.required]],
      image: ["", []],
    })

    this.offerList$ = this.offersService.getAllOffers()
  }

  public get externalId() {
    return this.createOfferForm?.get("externalId")
  }

  public get name() {
    return this.createOfferForm?.get("name")
  }

  public get category() {
    return this.createOfferForm?.get("category")
  }

  public get description() {
    return this.createOfferForm?.get("description")
  }

  public get price() {
    return this.createOfferForm?.get("price")
  }

  public get image() {
    return this.createOfferForm?.get("image")
  }

  createOffer() {
    const {
      externalId,
      name,
      category,
      description,
      price,
      image,
    } = this.createOfferForm?.value
    this.offersService.createOffer(
      externalId,
      name,
      category,
      description,
      price,
      image,)
      .subscribe(() => {
        this.createOfferForm?.reset()
        this._imagePreview = undefined
        this._snackBar.open('Успешно добавлено', 'Close', {
          duration: 3000
        });
      })
  }

  deleteOffer(externalId: number) {
    this.offersService.deleteOffer(externalId).subscribe(() => {
      this._snackBar.open(`Оффер № ${externalId} успешно удалён`, 'Close', {
        duration: 3000
      });
    });

  }

  public _onImagePicked(event: Event): void {
    const files = (event.target as HTMLInputElement).files;
    const file = files?.length && files[0];
    this.createOfferForm?.patchValue({image: file});
    this.createOfferForm?.get("image")?.updateValueAndValidity();
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        this._imagePreview = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  editOffer(externalId: number) {
    this.offerForEdit = this.offersService.getSingleOffer(externalId).subscribe(
      async (of) => {
        const response = await fetch(of.image);
        const blob = await response.blob();
        const file = new File([blob], 'image.jpg', {type: blob.type})
        this._imagePreview = of.image
        this.createOfferForm?.setValue({
          externalId: of.externalId,
          name: of.name,
          category: of.category,
          description: of.description,
          price: of.price,
          image: file,
        })
        this.isEdit = true
      }
    )
  }

  submitEditOffer(externalId: number) {
    const {
      name,
      category,
      description,
      price,
      image,
    } = this.createOfferForm?.value
    this.offersService.editOffer(name, category, description, price, image, externalId)
      .subscribe(() => {
        this.createOfferForm?.reset()
        this._imagePreview = undefined
        this._snackBar.open('Товар успешно обновлён', 'Close', {
          duration: 3000
        });
        this.isEdit = false
      })
  }

  clearEditForm() {
    this.createOfferForm?.reset()
    this._imagePreview = undefined
    this.isEdit = false
  }

}

