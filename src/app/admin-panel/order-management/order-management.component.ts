import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { PurchaseService } from "../../services/purchase.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FormBuilder, FormGroup } from '@angular/forms';
import { debounceTime, map, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-order-management',
  templateUrl: './order-management.component.html',
  styleUrls: ['./order-management.component.less']
})
export class OrderManagementComponent implements OnInit, AfterViewInit {

  @ViewChild('searchInput') searchInput: ElementRef | undefined;
  public searchForm: FormGroup | undefined
  public orderList$: Observable<any> | undefined;
  public purchaseTotal$: Observable<number> | undefined;
  public displayedColumns: string[] = ['name', 'email', 'status'];

  @ViewChild(MatPaginator) paginator?: MatPaginator;

  constructor(private purchaseService: PurchaseService,
              private _snackBar: MatSnackBar,
              private formBuilder: FormBuilder,) {
  }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      search: ["", []]
    })
    this.orderList$ = this.purchaseService.getPurchaseData$()
    this.purchaseTotal$ = this.purchaseService.getPurchaseTotal$()
  }

  ngAfterViewInit(): void {
    this.pageChange();
    this.initialLoad();
    fromEvent(this.searchInput?.nativeElement, 'keyup')
      .pipe(
        map((e: any) => e.target.value),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap((value: string) => this.purchaseService.getAllPurchase({
          currentPage: 1,
          limit: (this.paginator?.pageSize ?? 0),
          email: value
        }))
      )
      .subscribe();
  }

  initialLoad() {
    let currentPage = (this.paginator?.pageIndex ?? 0) + 1;
    this.purchaseService.getAllPurchase({currentPage: currentPage, limit: (this.paginator?.pageSize ?? 0)})
      .subscribe()
    this.orderList$ = this.purchaseService.getPurchaseData$()
    this.purchaseTotal$ = this.purchaseService.getPurchaseTotal$()
  }

  pageChange() {
    this.paginator?.page.pipe(
      switchMap(() => {
        let currentPage = (this.paginator?.pageIndex ?? 0) + 1;
        return this.purchaseService.getAllPurchase({currentPage: currentPage, limit: (this.paginator?.pageSize ?? 0)});
      })
    )
      .subscribe();
  }


  public changeStatus(_id: string, status: string) {
    this.purchaseService.changeStatus(_id, status).subscribe(
      () => this._snackBar.open('Статус обновлен', 'Close', {
        duration: 3000
      })
    )
  }

}
