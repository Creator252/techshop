import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminPanelComponent } from "./admin-panel.component";
import { OfferManagementComponent } from "./offer-management/offer-management.component";
import { OrderManagementComponent } from "./order-management/order-management.component";

const routes: Routes = [
  {
    path: '',
    component: AdminPanelComponent,
    pathMatch: 'prefix',
    children: [
      {path: 'offer-management', component: OfferManagementComponent},
      {path: 'order-management', component: OrderManagementComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPanelRoutingModule {
}
