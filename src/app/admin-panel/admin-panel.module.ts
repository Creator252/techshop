import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPanelComponent } from "./admin-panel.component";
import { OfferManagementComponent } from "./offer-management/offer-management.component";
import { ReactiveFormsModule } from "@angular/forms";
import { AdminPanelRoutingModule } from "./admin-panel-routing.module";
import { OrderManagementComponent } from './order-management/order-management.component';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [
    AdminPanelComponent,
    OfferManagementComponent,
    OrderManagementComponent,
  ],
  imports: [
    CommonModule,
    AdminPanelRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSidenavModule,
    MatTableModule,
    MatButtonModule,
    MatDividerModule,
    MatExpansionModule,
    MatPaginatorModule,
  ]
})
export class AdminPanelModule {
}
