import { Component, OnInit } from '@angular/core';
import {UserService} from "../services/user.service";
import { Observable } from 'rxjs';
import { FavoritesService } from '../services/favorites.service';
import { PurchaseService } from '../services/purchase.service';
import { OffersService } from '../services/offers.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {
  // @ts-ignore
 public user$ : Observable<any>;
 public purchases: any  = [];
 public activePurchase: any;

  selectOffer(id: Number) {
    this.offerService.selectOffer(id);
  }

  constructor(private userService: UserService, private favoritesService: FavoritesService, private purchaseService: PurchaseService, private offerService: OffersService
  ) { }

  public onSelectItem(product: any): void {
    this.activePurchase = product;
  }

  ngOnInit(): void {
    this.user$ = this.userService.getUserByEmail();
    this.purchaseService.autoComplete().subscribe((res)=>{
      this.purchases = res;
    });
  }

}
