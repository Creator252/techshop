import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { CartService } from "../services/cart.service";
import { OffersService } from "../services/offers.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.less']
})
export class CartComponent implements OnInit {

  //public cartData: any = {}
  //public cartTotal: number = 0
  public cartData: Observable<any> | undefined
  public cartTotal: Observable<number> | undefined

  constructor(private cartService: CartService,
              private offersService: OffersService,
  ) {
  }

  ngOnInit(): void {
    // this.cartService.cartData$.subscribe((data: any) => this.cartData = data)
    // this.cartService.cartTotal$.subscribe((total: number) => this.cartTotal = total)
    this.cartData = this.cartService.getCartData$()
    this.cartTotal = this.cartService.getCartTotal$()
  }

  updateQuantity(index: number, increase: boolean) {
    this.cartService.updateCartItems(index, increase)
  }

  deleteOfferFromCart(index: number) {
    this.cartService.deleteOfferFromCart(index)
  }

  deleteAllOffersFromCart() {
    this.cartService.deleteAllOffersFromCart()
  }

  selectOffer(id: Number) {
    this.offersService.selectOffer(id)
  }

}
