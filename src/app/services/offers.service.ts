import { Injectable } from '@angular/core';
import { OfferInterface, OfferServerResponse } from "../models/offer.model";
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { map, share, switchMap, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class OffersService {

  private SERVER_URL = environment.serverUrl
  private offerTrigger$ = new BehaviorSubject<void>(undefined);
  private offersData$ = new BehaviorSubject<any>(undefined);
  public currentCategory: string = "";

  constructor(private http: HttpClient,
              private router: Router,) {
    this.getAllOffers()
      .subscribe((data) => {
        this.offersData$.next(data)
      })

  }

  getOffersData$(): Observable<OfferInterface[]> {
    return this.offersData$
  }

  public getAllOffers(parameters?: any): Observable<OfferInterface[]> {

    return this.offerTrigger$.pipe(
      switchMap(() => {
        let params = new HttpParams()
        parameters?.category && (params = params.append("category", parameters?.category))
        parameters?.name && (params = params.append("name", parameters?.name))
        parameters?.maxPrice && (params = params.append("maxPrice", parameters?.maxPrice))
        parameters?.minPrice && (params = params.append("minPrice", parameters?.minPrice))
        parameters?.category ? this.currentCategory = parameters?.category : false
        return this.http.get<OfferServerResponse>(this.SERVER_URL + "/offers", {params})
      }),
      map((res) => res.offers),
      tap((res) => {
        this.offersData$.next(res)
      }),
      share(),
    )
  }

  public getSingleOffer(id: number): Observable<OfferInterface> {
    return this.http.get<OfferInterface>(this.SERVER_URL + "/offers/" + id)
  }

  public getOffersFromCategory(categoryName: string): Observable<OfferInterface> {
    return this.http.get<OfferInterface>(this.SERVER_URL + "/offers/category/" + categoryName)
  }

  public selectOffer(id: Number) {
    this.router.navigate(['/offers', id]).then()
  }

  //admin-panel
  public deleteOffer(externalId: number): Observable<any> {
    return this.http.delete<any>(this.SERVER_URL + `/offers/delete/${externalId}`)
      .pipe(
        tap(() => this.offerTrigger$.next())
      )
  }

  public createOffer(externalId: number,
                     name: string,
                     category: string,
                     description: string,
                     price: number,
                     image: File): Observable<{ message: string }> {
    const formData: FormData = new FormData();
    formData.append("externalId", externalId.toString())
    formData.append("name", name)
    formData.append("category", category)
    formData.append("description", description)
    formData.append("price", price.toString())
    formData.append("image", image, image.name)
    return this.http.post<{ message: string }>(this.SERVER_URL + "/offers", formData)
      .pipe(
        tap(({message}) => {
          console.log("message", message)
          this.offerTrigger$.next()
        })
      )
  }

  public editOffer(name: string,
                   category: string,
                   description: string,
                   price: number,
                   image: File,
                   externalId: number) {
    const formData: FormData = new FormData()
    formData.append("name", name)
    formData.append("category", category)
    formData.append("description", description)
    formData.append("price", price.toString())
    formData.append("image", image, image.name)
    return this.http.put<{ message: string }>(this.SERVER_URL + `/offers/edit/${externalId}`, formData)
      .pipe(
        tap(({message}) => {
          console.log("message", message)
          this.offerTrigger$.next()
        })
      )
  }

}
