import { Injectable } from '@angular/core';
import { FavoriteInterface } from '../models/FavoriteInterface.model'
import { CartService } from './cart.service';
import { TestBed } from '@angular/core/testing';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {

  constructor(private cartService: CartService) {
    this.favorites$.next(this.favorites);
   }

  public favorites: FavoriteInterface[] = JSON.parse(<string>localStorage.getItem('favorite'));
  public favoriteIds: number[] = JSON.parse(<string>localStorage.getItem('ids'));

  private favoriteTotal$ = new BehaviorSubject<number>(0);
  private favorites$ = new BehaviorSubject<any>(this.favorites);
  private favoriteIds$ = new BehaviorSubject<number[]>(this.favoriteIds);

  getFavoriteTotal$(): Observable<number>{
    return this.favoriteTotal$
  }

  getFavorites$(): Observable<FavoriteInterface[]>{
    return this.favorites$;
  }

  getFavoriteIds$(): Observable<number[]>{
    return this.favoriteIds$;
  }

  addFavorite(favorite: FavoriteInterface, id: number):number[] {
    if (this.favoriteIds.length == 0) {
      this.favoriteIds.push(id);
      this.favorites.push(favorite);
    }else if( !this.favoriteIds.includes(id)){
      this.favoriteIds.push(id);
      this.favorites.push(favorite)
    }
    localStorage.setItem('favorite', JSON.stringify(this.favorites));
    localStorage.setItem('ids', JSON.stringify(this.favoriteIds));

    this.favorites$.next(this.favorites);
    this.favoriteIds$.next(this.favoriteIds);

    return this.favoriteIds;
  }

  delFavorite(favorite: FavoriteInterface, id: number):void{
    let index = this.favoriteIds.indexOf(id);
    this.favoriteIds.splice(index, 1);
    this.favorites.splice(index, 1);

    localStorage.setItem('favorite', JSON.stringify(this.favorites));
    localStorage.setItem('ids', JSON.stringify(this.favoriteIds));

    this.favorites$.next(this.favorites);
    this.favoriteIds$.next(this.favoriteIds);
  }

  delAllFavorites():void {
    this.favorites = [];
    this.favoriteIds = []
    localStorage.setItem('favorite', JSON.stringify(this.favorites));
    localStorage.setItem('ids', JSON.stringify(this.favoriteIds));

    this.favorites$.next(this.favorites);
    this.favoriteIds$.next(this.favoriteIds);
  }
}
