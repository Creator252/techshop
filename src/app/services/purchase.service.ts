import { Injectable } from '@angular/core';
import { map, tap, switchMap, share } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { PurchaseInterface, PurchaseServerResponse } from "../models/purchase.model";

// interface PurchaseResponse {
//  status: string,
//  region: string,
//  city: string,
//  street: string,
//  home: string,
//  housing: string,
//  comment: string,
//  name: string,
//  sname: string,
//  email: string
// }

@Injectable({
  providedIn: 'root'
})
export class PurchaseService {

  private SERVER_URL = environment.serverUrl;
  private purchaseTrigger$ = new BehaviorSubject<void>(undefined);
  private purchaseData$ = new BehaviorSubject<PurchaseInterface[]>([]);
  private purchaseTotal$ = new BehaviorSubject<number>(0);

  constructor(private http: HttpClient) {

  }

  getPurchaseData$(): Observable<PurchaseInterface[]> {
    return this.purchaseData$
  }

  getPurchaseTotal$(): Observable<number> {
    return this.purchaseTotal$
  }

  public postPurchase(
    status: string,
    totalPrice: number,
    region: string,
    city: string,
    street: string,
    home: string,
    housing: string,
    comment: string,
    name: string,
    sname: string,
    email: string,
    products: object,
    payment: string,
    cardNumber: string,
    cardDate: string,
    cardName: string,
    cardCvv: string
  ): Observable<any> {
    return this.http.post(`${this.SERVER_URL}/purchases/`, {
      status,
      totalPrice,
      region,
      city,
      street,
      home,
      housing,
      comment,
      name,
      sname,
      email,
      products,
      payment,
      cardNumber,
      cardDate,
      cardName,
      cardCvv
    })
  }

  public getCities(): Observable<any> {
    return this.http.get(this.SERVER_URL + '/cities');
  }

  public autoComplete(): Observable<any> {
    const email = localStorage.getItem("email");
    return this.http.get(this.SERVER_URL + `/purchases/${email}`);
  }

  public getAllPurchase(parameters?: any): Observable<PurchaseServerResponse> {
    return this.purchaseTrigger$.pipe(
      switchMap(() => {
          let params = new HttpParams()
          parameters?.email && (params = params.append("email", parameters?.email))
          parameters?.currentPage && (params = params.append("currentPage", parameters?.currentPage))
          parameters?.limit && (params = params.append("limit", parameters?.limit))
          return this.http.get<PurchaseServerResponse>(this.SERVER_URL + `/purchases/all`, {params})
        }
      ),
      tap((res) => {
        this.purchaseData$.next(res.purchase)
        this.purchaseTotal$.next(res.totalPurchase)
      }),
      share(),
    )
  }

  public changeStatus(_id: string, status: string): Observable<{ message: string }> {
    return this.http.put<{ message: string }>(this.SERVER_URL + `/purchases/editStatus/${_id}`, {status})
      .pipe(
        tap(() => this.purchaseTrigger$.next())
      )
  }
}
