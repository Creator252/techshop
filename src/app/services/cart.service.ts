import { OffersService } from './offers.service';
import { CartInterface } from '../models/cart.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { OfferInterface } from '../models/offer.model';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public cart: CartInterface = {
    total: 0,
    data: [{
      numInCart: 0,
      id: 0,
      subTotal: 0,
      offer: undefined
    }]
  }

  private cartTotal$ = new BehaviorSubject<number>(0)
  private cartData$ = new BehaviorSubject<CartInterface>(this.cart)

  getCartData$(): Observable<CartInterface> {
    return this.cartData$
  }

  getCartTotal$(): Observable<number> {
    return this.cartTotal$
  }

  constructor(private offersService: OffersService) {
    this.cartTotal$.next(this.cart.total);
    this.cartData$.next(this.cart)

    let info: any = JSON.parse(localStorage.getItem("cart")!)
    if (info !== null && info !== undefined && info.data[0].numInCart !== 0) {
      this.cart = info;

      this.cart.data.forEach((el: any) => {
        this.offersService.getSingleOffer(el.id).subscribe((actualOfferInfo: OfferInterface) => {
          this.calculateTotal()
          localStorage.setItem("cart", JSON.stringify(this.cart))
          this.cartData$.next({...this.cart})
        })
      })
    }

  }


  addToCart(id: number) {
    this.offersService.getSingleOffer(id).subscribe(el => {
      if (this.cart.data[0].offer === undefined) {
        this.cart.data[0].offer = el
        this.cart.data[0].numInCart = 1
        this.calculateTotal()
        this.calculateSubTotal(0)
        this.cart.data[0].id = el.externalId
        localStorage.setItem("cart", JSON.stringify(this.cart))
        this.cartData$.next({...this.cart})
      } else {
        let index = this.cart.data.findIndex((p: any) => p.offer?.externalId === el.externalId)

        if (index !== -1) {
          this.cart.data[index].numInCart++
          this.calculateTotal()
          this.calculateSubTotal(index)
          localStorage.setItem("cart", JSON.stringify(this.cart))
        } else {
          this.cart.data.push({
            numInCart: 1,
            id: el.externalId,
            subTotal: el.price,
            offer: el
          })
          this.calculateTotal()
          localStorage.setItem("cart", JSON.stringify(this.cart))
          this.cartData$.next({...this.cart})

        }
      }
    })
  }

  updateCartItems(index: number, increase: boolean) {
    let data = this.cart.data[index]

    if (increase) {
      data.numInCart++
      this.cart.data[index].numInCart = data.numInCart
      this.calculateTotal()
      this.calculateSubTotal(index)
      //localStorage.setItem("cart", JSON.stringify(this.cartDataClient))
      this.cartData$.next({...this.cart})
      localStorage.setItem("cart", JSON.stringify(this.cart))
    } else {
      data.numInCart--

      if (data.numInCart < 1) {
        this.deleteOfferFromCart(index)
        this.cartData$.next({...this.cart})
      } else {
        this.cartData$.next({...this.cart})
        this.cart.data[index].numInCart = data.numInCart
        this.calculateTotal()
        this.calculateSubTotal(index)
        localStorage.setItem("cart", JSON.stringify(this.cart))
      }
    }
  }

  deleteOfferFromCart(index: number) {
    this.cart.data.splice(index, 1)
    this.calculateTotal()

    if (this.cart.total === 0) {
      this.cart = {total: 0, data: [{numInCart: 0, id: 0, subTotal: 0, offer: undefined}]}
      localStorage.setItem("cart", JSON.stringify(this.cart))
      this.cartData$.next({...this.cart})
    } else {
      localStorage.setItem("cart", JSON.stringify(this.cart))
      this.cartData$.next({...this.cart})
    }
  }

  deleteAllOffersFromCart() {
    this.cart = {total: 0, data: [{numInCart: 0, id: 0, subTotal: 0, offer: undefined}]}
    localStorage.setItem("cart", JSON.stringify(this.cart))
    this.cartData$.next({...this.cart})
  }

  calculateTotal() {
    let Total = 0
    this.cart.data.forEach((p: any) => {
      const {numInCart} = p
      const {price}: any = p.offer
      Total += numInCart * price
    })
    this.cart.total = Total
    this.cartTotal$.next(this.cart.total)
  }

  calculateSubTotal(index: number) {
    let subTotal = 0;
    const p = this.cart.data[index]
    subTotal = p.offer!.price * p.numInCart
    this.cart.data[index].subTotal = subTotal
  }

}
