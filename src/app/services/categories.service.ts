import { Injectable } from '@angular/core';
import { CategoryModel } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  private categories: CategoryModel[] = [
    {
      nameOfCategory: "Ноутбуки",
      nameOfRouterCategory: "Laptop"
    },
    {
      nameOfCategory: "Телефоны",
      nameOfRouterCategory: "Mobile"
    },
    {
      nameOfCategory: "Наушники",
      nameOfRouterCategory: "Headphones"
    }]

  getCategories(): CategoryModel[] {
    return this.categories
  }

  constructor() {
  }

}
