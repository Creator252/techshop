import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, of } from 'rxjs';
import { tap, map, switchMap, share } from 'rxjs/operators';
import { environment } from "../../environments/environment";
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { UserResponse } from "../models/user.model";

interface LoginResponse {
  token: string,
  role: string,
  email: string,
  message?: string,
}


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private SERVER_URL = environment.serverUrl
  public userTrigger$ = new BehaviorSubject<void>(undefined)

  constructor(private http: HttpClient,
              private jwtHelper: JwtHelperService,
              private router: Router,
  ) {
  }

  public getUserByEmail(): Observable<UserResponse | null> {
    return this.userTrigger$.pipe(
      switchMap(() => {
        const email = localStorage.getItem("email")
        if (email) {
          return this.http.get<UserResponse>(`${this.SERVER_URL}/users/${email}`)
        } else {
          return of(null)
        }
      }),
      share(),
    )
  }

  public login(email: string, password: string): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${this.SERVER_URL}/users/login`, {
      email: email,
      password: password,
    }).pipe(
      tap((res) => {
        this.userTrigger$.next()
        return this.setUserData(res)
      })
    )
  }

  public getToken(): string | null {
    return localStorage.getItem("token");
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem("token")
    return !!token && !this.jwtHelper.isTokenExpired(token)
  }

  private setUserData(data: LoginResponse) {
    localStorage.setItem("token", data.token)
    localStorage.setItem('email', data.email)
    localStorage.setItem('role', data.role)
    this.userTrigger$.next()
  }

  public register(email: string, password: string): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${this.SERVER_URL}/users/register`, {
      email: email,
      password: password,
    }).pipe(
      tap((res) => {
        this.userTrigger$.next()
        return this.setUserData(res)
      })
    )
  }

  public logout() {
    localStorage.removeItem('token')
    localStorage.removeItem('email')
    localStorage.removeItem('role')
    this.userTrigger$.next()
    this.router.navigate([""])
  }

  public editUser(
    image: File,
    name: string = '',
    sname: string = '',
    region: string = '',
    city: string = '',
    street: string = '',
    home: string = '',
    housing: string = '',
    phone: string = '') {
    const formData: FormData = new FormData();
    formData.append("image", image, image.name);
    formData.append("name", name);
    formData.append("sname", sname);
    formData.append("region", region);
    formData.append("city", city);
    formData.append("street", street);
    formData.append("home", home);
    formData.append("housing", housing);
    formData.append("phone", phone);
    const email = localStorage.getItem("email");
    return this.http.put(`${this.SERVER_URL}/users/edit/${email}`, formData);

  }

}


