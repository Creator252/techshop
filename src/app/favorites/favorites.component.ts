import { Component, OnInit } from '@angular/core';
import { FavoritesService } from '../services/favorites.service';
import {FavoriteInterface} from '../models/FavoriteInterface.model'
import { CartService } from '../services/cart.service';
import { UserService } from '../services/user.service';
import { OfferInterface } from '../models/offer.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.less'],
})
export class FavoritesComponent implements OnInit {

  constructor(private favoritesService: FavoritesService, private cartService: CartService, private userService: UserService) { }

 public favoriteObservable: Observable<FavoriteInterface[]> | undefined;
//  public favoriteObservableTotal: Observable<number> | undefined;
 public favoriteIdsObservable: Observable<number[]> | undefined;

 public deleteInFutureItemList: FavoriteInterface[] = [];
 public deleteInFutureIdList: number[] = [];
 public isAuthenticated: boolean = this.userService.isAuthenticated();

  ngOnInit(): void {
      this.favoriteObservable = this.favoritesService.getFavorites$();
      this.favoriteIdsObservable = this.favoritesService.getFavoriteIds$();
  }
  ngOnDestroy():void{
    for(let i = 0; i< this.deleteInFutureIdList.length; i++){
      this.delFavorite(this.deleteInFutureItemList[i],this.deleteInFutureIdList[i]);
    }
  }

  addFavorite(item: OfferInterface, externalId: number):void{
    this.favoritesService.addFavorite(item,externalId);
    let index = this.deleteInFutureIdList.indexOf(externalId);
    this.deleteInFutureIdList.splice(index, 1);
    this.deleteInFutureItemList.splice(index, 1);
  }

  delFavorite(favorite: OfferInterface, id:number):void{
    this.favoritesService.delFavorite(favorite, id);
  }

  delOnHeart(favorite: OfferInterface, id:number):void{
    this.deleteInFutureItemList.push(favorite);
    this.deleteInFutureIdList.push(id);
  }

  delAllFavorites():void{
    return this.favoritesService.delAllFavorites()
  }

  addAllToCart(favorites: any):void{
    for(let i = 0; i < favorites.length; i++){
      this.cartService.addToCart(favorites[i].externalId);
    }
  }

  addToCart(id: number):void{
    return this.cartService.addToCart(id);
  }



}
