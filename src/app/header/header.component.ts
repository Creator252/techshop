import { Observable } from 'rxjs';
import { CartService } from '../services/cart.service';
import { Component, OnInit } from '@angular/core';
import { FavoritesService } from '../services/favorites.service';
import { FavoriteInterface } from '../models/FavoriteInterface.model';
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  public cartData$: Observable<any> | undefined
  public user$: Observable<any> | undefined
  public favorites:Observable<FavoriteInterface[]> | undefined;

  constructor(
    private cartService: CartService,
    private favoritesService: FavoritesService,
    private userService: UserService,
  ) {
  }


  ngOnInit(): void {
    this.cartData$ = this.cartService.getCartData$()
    this.user$ = this.userService.getUserByEmail()
    this.favorites = this.favoritesService.getFavorites$()
  }

  logout() {
    this.userService.logout()
    this.favoritesService.delAllFavorites()
  }

}
