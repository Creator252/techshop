import { CategoryModel } from '../models/category.model';
import { CategoriesService } from '../services/categories.service';
import { OffersService } from '../services/offers.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.less']
})
export class CategoryComponent implements OnInit {

  public categories: any[] = []

  constructor(private offersService: OffersService, private categoriesService: CategoriesService) {
  }

  getOffersFromCategory(category: string) {
    return this.offersService.getAllOffers({category: category})
      .subscribe()
  }

  getAllOffers() {
    return this.offersService.getAllOffers()
      .subscribe()
  }

  getCategories(): CategoryModel[] {
    return this.categoriesService.getCategories()
  }

  ngOnInit(): void {
    this.categories = this.getCategories()
  }

}
