import { OfferInterface } from './offer.model';

export interface CartData {
  numInCart: number,
  id: number,
  subTotal: number,
  offer: OfferInterface | undefined,
}

export interface CartInterface {
  total: number,
  // data: [{
  //   numInCart: number,
  //   id: number,
  //   subTotal: number,
  //   offer: OfferInterface | undefined,
  // }]
  data: CartData[]
}
