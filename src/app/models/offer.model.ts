export interface OfferInterface {
  _id: string,
  externalId: number,
  name: string,
  category: string,
  description: string,
  price: number,
  image: string,
}

export interface OfferServerResponse {
  message: string,
  offers: OfferInterface[]
}
