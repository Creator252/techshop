import { CartData } from "./cart.model";

export interface PurchaseInterface {
  _id: string,
  status: string,
  totalPrice: number,
  region: string,
  city: string,
  street: string,
  home: string,
  housing: string,
  comment: string,
  name: string,
  sname: string,
  email: string,
  products: CartData[],
  payment: string,
}

export interface PurchaseServerResponse {
  purchase: PurchaseInterface[],
  totalPurchase: number,
}
