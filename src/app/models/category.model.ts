export type CategoryModel = {
  nameOfCategory: string;
  nameOfRouterCategory: string;
}
