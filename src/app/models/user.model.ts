export interface UserResponse {
  token: string,
  email: string,
  userName: string,
  photo: string,
  role: string,
  password: string,
}




