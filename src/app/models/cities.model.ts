export interface CityInterfaceServer{
  name: string,
  regions: []
}

export interface CityInterfaceResponse{
  message:string,
  cities: CityInterfaceServer[]
}
