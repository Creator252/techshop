import { Injectable } from '@angular/core';
import {
  Router,
  ActivatedRouteSnapshot,
  CanActivate,
  CanDeactivate,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from "../services/user.service";

@Injectable({
  providedIn: 'root'
})
export class AdminPanelGuard implements CanActivate, CanDeactivate<unknown> {

  constructor(
    private userService: UserService,
    private router: Router,
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (localStorage.getItem('role') === 'admin') {
      return true;
    } else {
      if (localStorage.getItem('role') === 'user') {
        this.router.navigate(['profile'])
        return false
      } else {
        this.router.navigate(['sign-in'])
        return false
      }
    }
    return false
  }

  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (localStorage.getItem('role') === 'admin') {
      this.router.navigate(['admin-panel'])
      return false
    }
    return true;
  }

}
