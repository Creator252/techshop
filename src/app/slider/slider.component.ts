import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.less']
})
export class SliderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  imgCollection: Array<object> = [
      {
        image: '../../assets/img/slider_images/slider_image2.jpg',
        thumbImage: '../../assets/img/slider_images/slider_image2.jpg',
        alt: 'Image 2'
      }, {
        image: '../../assets/img/slider_images/slider_image3.jpg',
        thumbImage: '../../assets/img/slider_images/slider_image3.jpg',
        alt: 'Image 3'
      }, {
        image: '../../assets/img/slider_images/slider_image4.jpg',
        thumbImage: '../../assets/img/slider_images/slider_image4.jpg',
        alt: 'Image 4'
      }, {
        image: '../../assets/img/slider_images/slider_image5.jpg',
        thumbImage: '../../assets/img/slider_images/slider_image5.jpg',
        alt: 'Image 5'
      }
  ];

}

