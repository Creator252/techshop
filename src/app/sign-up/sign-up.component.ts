import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RxwebValidators } from "@rxweb/reactive-form-validators";
import { UserService } from "../services/user.service";
import { Router } from '@angular/router';
import { MatSnackBar } from "@angular/material/snack-bar";
import { emailValidator } from '../../app/user-info/email.validator';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.less']
})
export class SignUpComponent implements OnInit {

  public hide = true;
  public registrationForm?: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router,
              private _snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.registrationForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email, emailValidator.validEmails]],
      password: ["", [Validators.required, Validators.minLength(6)]],
      confirmPassword: ["", [RxwebValidators.compare({fieldName: 'password'})]],
    })
  }

  public get email() {
    return this.registrationForm?.get("email")
  }

  public get password() {
    return this.registrationForm?.get("password")
  }

  public get confirmPassword() {
    return this.registrationForm?.get("confirmPassword")
  }

  public signUp(): void {
    const {email, password} = this.registrationForm?.value
    this.userService.register(email, password).subscribe(() => {
        this.router.navigate(["profile"])
        this._snackBar.open('Вы зарегистрированы', 'Close', {
          duration: 3000
        });
      },
      () => {
        this._snackBar.open('Email занят', 'Close', {
          duration: 3000
        });
      })
  }


}
