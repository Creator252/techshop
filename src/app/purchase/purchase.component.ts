import { Component, OnInit } from '@angular/core';
import { FormControlName, ReactiveFormsModule, Validators,FormsModule } from '@angular/forms';
import { FormControl, FormGroup} from '@angular/forms';
import {CartService} from '../services/cart.service';
import { PurchaseService } from '../services/purchase.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {CartInterface} from '../models/cart.model'
import {UserService} from '../services/user.service'
import { Router } from '@angular/router';
import {emailValidator} from '../../app/user-info/email.validator';
import { HttpErrorResponse } from '@angular/common/http';

interface country{
  name:string,
  regions:Array<{
    name: string,
    cities: Array<{
      name: string,
      late: number,
      lng: number
    }>
  }>
}

interface userData{
  message: string,
  purchases:Array<{
    city: string,
    comment: string,
    email: string,
    home: string,
    housing: string,
    name: string,
    payment: string,
    products: Array<any>,
    region: string,
    sname: string,
    status: string,
    street: string,
    totalPrice: number
  }>
}

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.less']
})
export class PurchaseComponent implements OnInit {

 public purchaseForm: FormGroup
 public cityList: country | undefined;
 public regions: string[] = ["Брестская обл.","Витебская обл.","Гомельская обл.","Гродненская обл.","Минская обл.","Могилевская обл."]
 public cartProducts: CartInterface ={
  total: 0,
  data: [{
    numInCart: 0,
    id: 0,
    subTotal: 0,
    offer: undefined,
  }]
 };
 public products: any; // this.cartProducts.data
 public cities: string[] = [];
 public userData: userData | undefined;
 public isAutocomplete:boolean = false;
 public message = document.getElementsByClassName('message') as HTMLCollectionOf<HTMLElement>;
 public citiesFilter: Observable<any> | undefined;
 public user$: Observable<any> | undefined;
 public isVisa:boolean = false;
 public isMaster:boolean = false;
 public isAE:boolean = false;
 public isShow:boolean = true;

  constructor(private cartService: CartService, private purchaseService: PurchaseService, private _snackBar: MatSnackBar, private userService: UserService, private router: Router) {
    this.purchaseForm = new FormGroup({
      region: new FormControl(null, [Validators.required]),
      city: new FormControl(null, [Validators.required]),

      Address: new FormGroup({
        street: new FormControl(null, [Validators.required,Validators.pattern(/^[a-zA-Z]*|[а-яА-Я]*$/)]),
        home: new FormControl(null, [Validators.required]),
        housing: new FormControl(null)
      }),

      comment: new FormControl(null),

      Contacts: new FormGroup({
        name: new FormControl(null,[Validators.required, Validators.pattern(/^[a-zA-Z]*|[а-яА-Я]*$/)]),
        sname: new FormControl(null,[Validators.required, Validators.pattern(/^[a-zA-Z]*|[а-яА-Я]*$/)] ),
        mail:new FormControl(null,[Validators.required, Validators.email,emailValidator.validEmails]),
      }),
      PaymentVal: new FormGroup({
        pay: new FormControl('card')
      }),
      Card: new FormGroup({
        cardNumber: new FormControl(null),
        cardDate: new FormControl(null),
        cardName: new FormControl(null, [Validators.maxLength(25),Validators.pattern(/^([A-Za-z\.\-]{1,23}) ([A-Za-z\.\-]{1,23})$/)]),
        cvv: new FormControl(null)
      })
    });
  }

  get _region(){
    return this.purchaseForm?.get('region');
  }

  get _city(){
    return this.purchaseForm?.get('city');
  }

  get _street(){
    return this.purchaseForm?.get('Address')?.get('street');
  }

  get _home(){
    return this.purchaseForm?.get('Address')?.get('home');
  }

  get _housing(){
    return this.purchaseForm?.get('Address')?.get('housing');
  }

  get _name(){
    return this.purchaseForm?.get('Contacts')?.get('name');
  }

  get _sname(){
    return this.purchaseForm?.get('Contacts')?.get('sname');
  }

  get _mail(){
    return this.purchaseForm?.get('Contacts')?.get('mail');
  }

  get _cardNumber(){
    return this.purchaseForm?.get('Card')?.get('cardNumber');
  }

  get _cardDate(){
    return this.purchaseForm?.get('Card')?.get('cardDate');
  }

  get _cardName(){
    return this.purchaseForm?.get('Card')?.get('cardName');
  }

  getCities(region:string): void {
    if(this.instanceOfCountry(this.cityList)){
    for(let i = 0; i < this.cityList.regions.length; i++){
      if(this.cityList.regions[i].name == region){
        for(let j = 0; j < this.cityList.regions[i].cities.length; j++){
          this.cities.push(this.cityList.regions[i].cities[j].name)
        }
      }
    }
  }
  }

  toUpper():void{
    this.purchaseForm.value.Card.cardName = this.purchaseForm.value.Card.cardName.toUpperCase();
  }
  instanceOfCountry(cityList: any): cityList is country {
    return 'name' in cityList;
  }

  instanceOfUserData(userData: any): userData is country {
    return 'message' in userData;
  }

  printNamesInCart():void{
    let cartProducts:any = localStorage.getItem('cart');
    this.cartProducts = JSON.parse(cartProducts);
    this.products = this.cartProducts.data;
  }

  public postPurchase():void{
    this.purchaseService.postPurchase(
      'Оформлен',
      this.cartProducts.total,
      this.purchaseForm?.value.region,
      this.purchaseForm?.value.city,
      this.purchaseForm?.value.Address.street,
      this.purchaseForm?.value.Address.home,
      this.purchaseForm?.value.Address.housing,
      this.purchaseForm?.value.comment,
      this.purchaseForm?.value.Contacts.name,
      this.purchaseForm?.value.Contacts.sname,
      this.purchaseForm?.value.Contacts.mail,
      this.products,
      this.purchaseForm?.value.PaymentVal.pay,
      this.purchaseForm?.value.Card.cardNumber,
      this.purchaseForm?.value.Card.cardDate,
      this.purchaseForm?.value.Card.cardName,
      this.purchaseForm?.value.Card.cvv,

      ).subscribe((data:any)=> {
        if(data.message == 'order created'){
          this.router.navigate(["profile"])
        }
        this.purchaseForm?.reset();
      this._snackBar.open('Заказ оформлен', 'Ok',{ duration: 3000})
      });

  }

  public autoComplete():void{
    if(this.isAutocomplete && this.userData!= undefined && this.instanceOfUserData(this.userData)){
      let length = this.userData.purchases.length - 1;
      this.purchaseForm.patchValue({
        region: this.userData.purchases[length].region,
        city: this.userData.purchases[length].city,
        Address: {
          street: this.userData.purchases[length].street,
          home: this.userData.purchases[length].home,
          housing: this.userData.purchases[length].housing
        },
        Contacts:{
          name: this.userData.purchases[length].name,
          sname: this.userData.purchases[length].sname,
          mail: this.userData.purchases[length].email
        }
      });
    }
  }

  showUsername():string | undefined{
    return localStorage.getItem('email')?.split('@')[0];
  }

  showMessage(isShow:boolean):void{
    if(localStorage.getItem('email')?.length && this.instanceOfUserData(this.userData) && isShow){
      this.isAutocomplete = true;
      this.showHideMessage('10px');
    }

  }

  showHideMessage(position: string):void{
    this.message[0].style.bottom = `${position}`;
}

  updateQuantity(index: number, increase: boolean):void {
  this.cartService.updateCartItems(index, increase)
}

  delAllAfterPurchase():void{
    this.cartService.deleteAllOffersFromCart();
  }

  typeOfCard():void{
    switch(this.purchaseForm?.value.Card.cardNumber.substring(0,1)){
      case '3':
        this.isAE = true;
        this.isMaster = false;
        this.isVisa = false;
        break;
      case '4':
        this.isVisa = true;
        this.isMaster = false;
        this.isAE = false;
        break;
      case '5':
        this.isMaster = true;
        this.isVisa = false;
        this.isAE = false;
        break;
      default:
        this.isVisa = false;
        this.isMaster = false;
        this.isAE = false;
    };
  }

public _filter(value: string): string[]{
  if(value != null){
  return this.cities.filter((city: string) => city.toLowerCase().includes(value.toLowerCase()));
  }
  return [];
}

public cashSelected(){
      this.purchaseForm.patchValue({
        Card:{
          cardNumber: '',
          cardDate: '',
          cardName: '',
          cvv: ''
        }
      });
  console.log('test')
}

  ngOnInit(): void {
    this.printNamesInCart();
    this.purchaseService.getCities().subscribe((res)=>{
      let cities:any = res;
      this.cityList = cities.cities[0];
    })
    this.purchaseService.autoComplete().subscribe(
      (data: any)=>{
      let userData:any = data;
      this.userData = userData;
      },
      (error: any) => {
        if (error instanceof HttpErrorResponse) {
          if (error.error instanceof ErrorEvent) {
              console.error("Error Event");
          } else{
        switch (error.status){
          case 404:
            this._snackBar.open('Поздравляем, это ваш первый заказ', 'Ok',{ duration: 3000});
            this.userData ={message:'',
                            purchases: []};
            this.isShow = false;
            break;
        }
       }
      }
    }
    );
    setTimeout (() => {
      this.showMessage(this.isShow);
   }, 1000);
   this.citiesFilter = this.purchaseForm.get('city')?.valueChanges.pipe(
    startWith(''),
    map(value => this._filter(value)),
   )
   this.user$ = this.userService.getUserByEmail();
    this.user$?.forEach(user => this.purchaseForm.patchValue({
      region: user.region,
      city: user.city,
      Address: {
        street: user.street,
        home: user.home,
        housing: user.housing
      },
      Contacts:{
        name: user.name,
        sname: user.sname,
        mail: user.email
        }
    }));
  }
}
