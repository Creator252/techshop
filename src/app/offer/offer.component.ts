import { CartService } from '../services/cart.service';
import { OffersService } from '../services/offers.service';
import { OfferInterface } from '../models/offer.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FavoritesService } from '../services/favorites.service';
import { UserService } from '../services/user.service';
import { FavoriteInterface } from '../models/FavoriteInterface.model';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.less']
})
export class OfferComponent implements OnInit {

  public id: number = 0
  public offer: OfferInterface = {} as OfferInterface
  public counter: number = 1
  public favoriteIds: number[] = this.favoritesService.favoriteIds;

  constructor(
    private favoritesService: FavoritesService,
    private userService: UserService,
    private offersService: OffersService,
    private cartService: CartService,
    private activateRoute: ActivatedRoute,
    private _snackBar: MatSnackBar,
  ) {
  }

  ngOnInit(): void {
    this.activateRoute.paramMap
      .pipe(
        map((param: ParamMap) => {
          return param.getAll("id")
        })
      ).subscribe(offerId => {
      this.id = +offerId
      this.offersService.getSingleOffer(this.id).subscribe(offer => {
        this.offer = offer
      })
    })
  }

  increment() {
    this.counter++;
  }

  decrement() {
    this.counter > 1 ? this.counter-- : this.counter
  }

  addToCart(id: number, qty: number) {
    qty = this.counter
    for (let i = 0; i < qty; i++) {
      this.cartService.addToCart(id)
    }
    this._snackBar.open('Добавлено', 'ok', {
      duration: 2000,
    });
  }

  addFavorite(favorite: any, id: any) {
    if (this.userService.isAuthenticated()) {
      this.favoriteIds = this.favoritesService.addFavorite(favorite, id);
      this._snackBar.open('Добавлено', 'ok', {
        duration: 2000,
      });
    } else {
      this._snackBar.open('Избранное доступно только зарегистрированным пользователям', 'ok', {
        duration: 3500,
      })
    }
  }

  delFavorite(favorite: any, id: any) {
    this.favoritesService.delFavorite(favorite, id)
    this._snackBar.open('Удалено', 'ok', {
      duration: 3500,
    })
  }

  toggleFavorite(item: FavoriteInterface, externalId: number): void {
    if (this.favoriteIds.includes(externalId)) {
      this.delFavorite(item, externalId);
    } else {
      this.addFavorite(item, externalId);
    }
  }

}
