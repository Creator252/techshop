import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from "./cart/cart.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { MainComponent } from "./main/main.component";
import { SignInComponent } from "./sign-in/sign-in.component";
import { SignUpComponent } from './sign-up/sign-up.component';
import { PurchaseComponent } from './purchase/purchase.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { OfferComponent } from './offer/offer.component';
import { ProfileComponent } from "./profile/profile.component";
import { ProfileGuard } from "./guards/profile.guard";
import { AdminPanelGuard } from "./guards/admin-panel.guard";
import { UserInfoComponent } from './user-info/user-info.component';

const routes: Routes = [
  {path: '', component: MainComponent},
  {
    path: 'admin-panel',
    loadChildren: () => import("./admin-panel/admin-panel.module").then(m => m.AdminPanelModule),
    canActivate: [AdminPanelGuard],
    canDeactivate: [AdminPanelGuard],
  },
  {path: 'cart', component: CartComponent},
  {path: 'favorites', component: FavoritesComponent},
  {path: 'sign-in', component: SignInComponent},
  {path: 'sign-up', component: SignUpComponent},
  {path: 'purchase', component: PurchaseComponent},
  {path: 'offers/:id', component: OfferComponent},
  {path: 'user-info', component: UserInfoComponent, canActivate: [ProfileGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [ProfileGuard]},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
