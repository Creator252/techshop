import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { fromEvent, Observable } from 'rxjs';
import { OffersService } from "../services/offers.service";
import { debounceTime, map, distinctUntilChanged, switchMap, tap, mergeMap } from 'rxjs/operators';
import { Options } from '@angular-slider/ngx-slider';
import { OfferInterface } from '../models/offer.model';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.less']
})
export class FilterComponent implements OnInit, AfterViewInit {

  @ViewChild('searchInput') searchInput: ElementRef | undefined;

  public filterForm: FormGroup | undefined
  public offerList$: Observable<any> | undefined;
  public panelOpenState = false;
  public offersData: OfferInterface[] | undefined;

  constructor(private formBuilder: FormBuilder,
              private offersService: OffersService,
  ) {
  }

  ngOnInit(): void {
    this.filterForm = this.formBuilder.group({
      search: ["", []],
      sliderControl: [[159, 9999], []]
    })
    this.offerList$ = this.offersService.getOffersData$()
  }

  ngAfterViewInit(): void {
    fromEvent(this.searchInput?.nativeElement, 'keyup')
      .pipe(
        map((e: any) => e.target.value),
        debounceTime(1000),
        distinctUntilChanged(),
        mergeMap((value: string) => this.offersService.getAllOffers({
          name: value,
          category: this.offersService.currentCategory
        }))
      )
      .subscribe();
  }

  public get search() {
    return this.filterForm?.get("search")
  }

  options: Options = {
    floor: 159,
    ceil: 9999,
    step: 1
  };

  getAllOffers() {
    return this.offersService.getAllOffers().subscribe()
  }

  searchByPrice() {
    let [min, max] = this.filterForm?.value.sliderControl
    this.offersService.getAllOffers({
      minPrice: min,
      maxPrice: max,
      category: this.offersService.currentCategory,
    }).subscribe()
  }

}
